const getRandomArrayItem = require("./helpers/getRandomArrayItem");
const citys = require("./data");
const axios = require('axios');
const config = require('./config');

const redis = require("redis");
const client = redis.createClient({
    host: "localhost",
    port: 6379
});


// Function que busca en la cache "city"
function getCityFromCache(city) {
    return new Promise((resolve, reject) => {
        // console.log("Buscando en cache: ", city)
        client.get(city, (error, rep) => {
            if (error) {
                reject(error);
            }
            console.log(rep)
            resolve(rep);
        });
    });
};


// Viene aquí por cada ciudad y consulta la temperatura.
const getCityTemp = async (city) => {
    const tempCache = await getCityFromCache(city); // Llamo la funcion para revisar si existe el dato en cache.

    if (tempCache == null) { 
        const apiKey = config.apikey;
        const baseApi = 'http://api.openweathermap.org'

        const respuesta = await axios.get(`${baseApi}/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`);
        client.set(city, JSON.stringify(respuesta.data.main.temp), 'EX', 600); // Guarda en cache ciudad:temp
        return respuesta.data.main.temp;
    } else {
        console.log(tempCache);
        return tempCache; // Si existe la ciudad en cache, devuelve su temperatura.
    }
}



async function climaController(req, res) {
    let arrayCitys = [];

    let eliminarRepetidos = arr => [...new Set(arr)];
    let newArrayCitys = eliminarRepetidos(arrayCitys);


    while (newArrayCitys.length < 3) {
        const randomCity = getRandomArrayItem(citys);
        newArrayCitys.push(randomCity);
        newArrayCitys = eliminarRepetidos(newArrayCitys);
    };

    try {
        for await (let city of newArrayCitys) {
            city.temperatura = await getCityTemp(city.name);
        }
        res.status(200).json({ "response": newArrayCitys });
    } catch (error) {
        res.status(500).json({ "response": error.message });
    }




    // newArrayCitys[0].temperatura = await getCityTemp(newArrayCitys[0].name);
    // newArrayCitys[1].temperatura = await getCityTemp(newArrayCitys[1].name);
    // newArrayCitys[2].temperatura = await getCityTemp(newArrayCitys[2].name);




    // axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[0].name}&appid=87bae2ffee43eec860796931e362d3ad&units=metric`)
    //     .then((respuesta) => {
    //         newArrayCitys[0].temperatura = respuesta.data.main.temp;
    //         axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[1].name}&appid=87bae2ffee43eec860796931e362d3ad&units=metric`)
    //             .then((respuesta) => {
    //                 newArrayCitys[1].temperatura = respuesta.data.main.temp;
    //                 axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[2].name}&appid=87bae2ffee43eec860796931e362d3ad&units=metric`)
    //                     .then((respuesta) => {
    //                         newArrayCitys[2].temperatura = respuesta.data.main.temp;
    //                         res.status(200).json({ "response": newArrayCitys });
    //                     })
    //             })
    //     })
    //     .catch((error) => {
    //         console.log(error);
    //         res.status(500).json('no se pudo procesar la solicitud);
    //     })
};

module.exports = climaController;