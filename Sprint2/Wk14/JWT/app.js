const config = require('./config');
const express = require('express');
const app = express();
app.use(express.json());
const { Router } = require('express');
const router = Router();
const jwt = require('jsonwebtoken');
// const { response } = require('express');
// const { nextTick } = require('process');



router.get('/', (req, res) => { res.json('Hola mundo!') });

// Importación de rutas
const auth = require('./routes/auth');

app.use(router);
app.use(auth);


//-------------------------------------------------
app.set('port', config.port);
app.listen(app.get('port'), function (err) {
    if (err) {
        console.error(err);
    } else {
        console.log('Server listening on port ' + app.get('port'));
    }
})