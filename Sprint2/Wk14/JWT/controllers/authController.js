const jwt = require('jsonwebtoken');
const config = require('../config');


// Login
exports.signin = function signin(req, res, next) {
    try {
        // TODO: Implementar acceso a base de datos
        const { usuario, password, email } = req.body;
        console.log('Logeado: ', { usuario, password, email});

        // TODO: Verificar credenciales de usuario en base de datos
        if (usuario != 'pablo' || password != '1234') {
            console.error("Error de credenciales: ");
            res.status(401).send({ status: 'Error de credenciales. Acceso denegado' });
        }

        jwt.sign(req.body, config.secretKey, { expiresIn: config.expiresIn }, (err, token) => {
            if (err) {
                console.error("Error interno: " + err.message);
                res.status(500).send({ status: 'Error interno' })
            } else {
                req.token = token;
                res.json({ status: 'Logeado', token });
            }
        });
    }
    catch (err) {
        console.error("Error interno: " + err.message);
        res.status(500).send({ status: 'Error interno' });
    }
};

// Registro
exports.signup = function signup(req, res, next) {
    try {
        // TODO: Implementar acceso a base de datos
        const { usuario, password, email } = req.body;
        console.log('Registrado: ', {usuario, password, email});

        jwt.sign(req.body, config.secretKey, { expiresIn: config.expiresIn }, (err, token) => {
            if (err) {
                console.error("Error interno: " + err.message);
                res.status(500).send({ status: 'Error interno' })
            } else {
                req.token = token;
                res.json({ status: 'Registrado', token });
            }
        });
    }
    catch (err) {
        console.error("Error interno: " + err.message);
        res.status(500).send({ status: 'Error interno' });
    }
};

exports.authenticated = function authenticated(req, res, next) {
    // TODO: Implementar acceso a base de datos
    // NOTE: Requiere que la petición incluye en el campo headers una clave (key) de la forma
    //       Bearer {token}, donde este token haya sido suministrado por signin o signup
    try {
        if (!req.headers.authorization) {
            console.error("Acceso denegado por falta de información de autorización");
            res.status(403).send({ status: 'Acceso denegado' })
        } else {
            const token = req.headers.authorization.split(' ')[1]
            jwt.verify(token, config.secretKey, (err, authData) => {
                console.log(authData)
                if (err) {
                    console.error("Acceso denegado: " + err.message);
                    res.status(403).send({ status: 'Acceso denegado' })
                } else {
                    req.authData = authData;
                    next();
                }
            });
        }
    }
    catch (err) {
        console.error("Error interno: " + err.message);
        res.status(500).send({ status: 'Error interno' });
    }
};

// Perfil de usuario
exports.me = function me(req, res, next) {
    res.json({ status: 'me', 'data': req.authData });
};