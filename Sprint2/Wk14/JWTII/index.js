const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
app.use(express.json());
const env = require('./.env')
require('dotenv').config();


app.post('/login', (req, res) => {

    const validEmail = "pablo@gmail.com";
    const validPassword = "123456"

    const { email, password } = req.body;

    if (validEmail === email && validPassword === password) {

        const token = jwt.sign({ email }, process.env.SECRET_KEY)
        res.json({ data: token })

    } else {
        res.status(403).send({
            message: "Credenciales invalidas"
        })
    }
    console.log({ email, password })
});



const authMiddleware = (req, res, next) => {
    const jwtToken = req.headers.authorization;
    // console.log(jwtToken)

    const token = jwtToken.split(" ")[1];

    try {
        const decoded = jwt.verify(token, process.env.SECRET_KEY)
        console.log(token)
        req.userEmail = decoded.email;
        next();

    } catch (error) {
        res.status(403).json({ data: "Error de token" })
    }
}




app.get('/ruta-segura', authMiddleware, (req, res) => {
    res.json({ data: `Bienvenido ${req.userEmail}` })
});




//--------------------------------------------
app.listen(3000, function () {
    console.log("------------------------------------")
    console.log("Servidor iniciado en el puerto: 3000")
    console.log("------------------------------------")
})