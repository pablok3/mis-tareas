const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
const env = require('./.env');

// const secretKey = process.env.SECRET_KEY;
const secretKey = "1234abcd";





app.post('/api/login', (req, res) => {
    const user = {
        id: 1,
        nombre: "Pablo",
        email: "pablo@gmail.com"
    }

    jwt.sign({ user }, secretKey, {expiresIn: '32s'}, (err, token) => {
        res.json({
            token
        }); 
    });
});



app.post('/api/posts', verifyToken, (req, res) => {

    jwt.verify(req.token, secretKey, (err, authData) => {

        if (err) {
            res.sendStatus(403);
        } else {
            res.json({
                mensaje: "Post fue creado",
                authData
            })
        }
    });
});




// Authorization: Bearer <token>
function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization']

    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }
}


//--------------------------------------------
app.listen(3000, function () {
    console.log("NodeJS corriendo en el puerto 3000")
})