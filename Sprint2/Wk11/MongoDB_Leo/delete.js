const mongoose = require('mongoose');

const Pelicula = mongoose.model('peliculas', {
    titulo: String,
    director: String,
    genero: String,
    lanzamiento: Date
})


Pelicula.deleteOne({ titulo: 'Titanic' })
    .then((res) => {
        console.log('La pelicula encontrada es:')
        console.log(res)
    });