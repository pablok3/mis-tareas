require('dotenv').config();
const env = require('./.env')

const config = {
    port: process.env.NODE_PORT,
    secretKey: process.env.SECRET_KEY,
    expiresIn: process.env.JWT_EXPIRES_IN
}

module.exports = config;