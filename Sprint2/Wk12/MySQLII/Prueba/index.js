const config = require('./src/config/config');
const express = require('express');
const app = express();
app.use(express.json())

const sequelize = require('./src/connection/sequelize');
const { getTareas, getBandas } = require('./src/repositories/tareas');


// Puerto ...............................
app.listen(config.node_port, () => console.log(`Escuchando puerto: ${config.node_port}`))

// sequelize.authenticate()
//     .then(() => {
//         console.log('Conexion establecida')
// })



app.get('/tareas', async (req, res) => {
    const { terminada } = req.query;
    const tareas = await getTareas(terminada);
    res.json({ tareas })
})

app.post('/tareas', async (req, res) => {
    const { titulo, descripcion } = req.body;

    const tarea = await sequelize.query(`INSERT INTO tareas (titulo, descripcion) VALUES ("${titulo}", "${descripcion}")`,
        { type: sequelize.QueryTypes.INSERT });
    res.json({
        tarea
    })
});


app.get('/bandas', async (req, res) => {
    const bandas = await getBandas();
    res.json({ bandas })
});

app.post('/bandas', async (req, res) => {
    const { titulo, descripcion } = req.body;

    const banda = await sequelize.query(`INSERT INTO tareas (titulo, descripcion) VALUES ("${titulo}", "${descripcion}")`,
        { type: sequelize.QueryTypes.INSERT });
    res.json({
        banda
    })
});



















// sequelize.query('SELECT * FROM canciones',
//     { type: sequelize.QueryTypes.SELECT }
// ).then(function (resultados) {
//     console.log(resultados)
// });



// sequelize.query('SELECT nombre FROM canciones WHERE nombre = El Rebelde',
//     { replacements: ['active'], type: sequelize.QueryTypes.SELECT }
// ).then(projects => {
//     console.log(projects)
// });



// sequelize.query('SELECT * FROM table WHERE estado = :estado',
//     { replacements: { estado: 'activo' }, type: sequelize.QueryTypes.SELECT }
// ).then(projects => {
//     console.log(projects)
// });