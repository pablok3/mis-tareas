const config = require('../config/config');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(`mariadb://${config.user}:${config.pass}@${config.host}:3306/acamica`);

module.exports = sequelize;