const  sequelize  = require("../connection/sequelize")


exports.getTareas = async (terminada) => {

    let tareas

    if (terminada == 'true') {
        tareas = await sequelize.query('SELECT * FROM tareas WHERE terminada = true', { type: sequelize.QueryTypes.SELECT })
    } else {
        tareas = await sequelize.query('SELECT * FROM tareas', { type: sequelize.QueryTypes.SELECT })
    }
    return tareas
}


exports.getBandas = async () => {
    const bandas = await sequelize.query('SELECT * FROM bandasmusicales ', { type: sequelize.QueryTypes.SELECT })
    return bandas
}


exports.postBandas = async () => {
    const bandas = await sequelize.query('SELECT * FROM bandasmusicales ', { type: sequelize.QueryTypes.INSERT })
    return bandas
}