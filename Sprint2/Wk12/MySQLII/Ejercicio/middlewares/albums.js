const  sequelize  = require("../src/connection/sequelize")

function validateData(req, res, next) {
    const { nombre, banda, fecha_publicacion} = req.body
    if(nombre != undefined && banda != undefined && fecha_publicacion != undefined){
        next()
    }else{
        res.status(400).json({"msg": "Tiene que ingresar nombre, banda y la fecha de publicacion"})
    }
}

async function validateIdParams(req, res, next){
    const idAlbum = req.params.idAlbum
    const album = await sequelize.query(`SELECT id FROM albumes WHERE id = ${idAlbum}`, { type : sequelize.QueryTypes.SELECT })
    if(!album[0]){
        res.status(400).json({"msg": "El id ingresado no pertenece a un album"})
    } else{
        next();
    }
}

async function validateIdBand(req, res ,next){
    const idBand = req.body.banda
    const band = await sequelize.query(`SELECT id FROM bandasmusicales WHERE id = ${idBand}`, { type : sequelize.QueryTypes.SELECT })
    if(band[0]){
        next()
    } else{
        res.status(400).json({"msg": "El id de la banda ingresada no pertenece a uno existente"})
    }

}




module.exports = {
    validateData,
    validateIdParams,
    validateIdBand
}