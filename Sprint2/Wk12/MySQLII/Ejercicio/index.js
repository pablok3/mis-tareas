const express = require('express');
const app = express();
const config = require('./src/config/config');
const cors = require('cors')

app.use(cors());
app.use(express.json())
app.use(express.urlencoded({extended: false}))  

// Routes ...............................

const bands = require('./routes/bands')
app.use('/bands', bands)

const albums = require('./routes/albums')
app.use('/albums', albums)

const songs = require('./routes/songs')
app.use('/songs', songs)

// Puerto ...............................
app.listen(config.node_port, () => console.log(`Escuchando puerto: ${config.node_port}`))
