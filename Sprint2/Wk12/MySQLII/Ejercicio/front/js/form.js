eventListener();

function eventListener() {
    document.querySelector('#formulario').addEventListener('submit', submitBand);
}

function submitBand(e) {
    e.preventDefault();
    const nombre = document.querySelector('#nombre').value; 
    const integrantes = parseInt(document.querySelector('#integrantes').value); 
    const fechaInicio = document.querySelector('#fecha-inicio').value; 
    const fechaSep = document.querySelector('#fecha-sep').value; 
    const pais = document.querySelector('#pais').value;

    if (!nombre || !integrantes || !fechaInicio || !pais) {
        alert("Completar los campos obligatorios")
    } else {
        const datos = new FormData();
        datos.append('nombre', nombre);
        datos.append('integrantes', integrantes);
        datos.append('fecha_inicio', fechaInicio);
        datos.append('fecha_separacion', fechaSep);
        datos.append('pais', pais);
        
        console.log(datos);


        const url = 'http://localhost:3000/bands';
        const http = new XMLHttpRequest();
    
        http.open("POST", url, true);
    
        http.onload = function() {
            if (this.status === 200) {
                const respuesta = JSON.parse(http.responseText);

            } else {
                alert("Hubo un error")
            }
        }
    http.send(datos);
    }
}