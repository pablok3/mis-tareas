async function getBands() {
    const response = await fetch('http://localhost:3000/bands');
    const bands = await response.json();
    return bands
}

async function getAlbums() {
    const response = await fetch('http://localhost:3000/albums');
    const bands = await response.json();
    return bands
}
async function getSongs() {
    const response = await fetch('http://localhost:3000/songs');
    const bands = await response.json();
    return bands
}
async function postBand() {
    const response = await fetch('http://localhost:3000/songs');
    const bands = await response.json();
    return bands
}

document.querySelector('#all-bands').addEventListener('click', async function () {
    const {bands} = await getBands();
    let registro = "";
    for (let i = 0; i < bands.length; i++) {
    registro +=     `
                        
                            <tr scope="row">
                                <td>
                                    <p>${bands[i].nombre}</p>
                                </td>
                                <td>
                                    <p>${bands[i].integrantes}</p>
                                </td>
                                <td>
                                    <p>${bands[i].fecha_inicio}</p>
                                </td>
                                <td>
                                    <p>${bands[i].fecha_separacion}</p>
                                </td>
                                <td>
                                    <p>${bands[i].pais}</p>
                                </td>
                            </tr>
                        `;
        
    }
    document.querySelector('#table-songs').classList.add('disable');
    document.querySelector('#table-albums').classList.add('disable');
    document.querySelector('#table-bands').classList.remove('disable');

    document.querySelector('#body-bands').innerHTML = registro;
})

document.querySelector('#all-albums').addEventListener('click', async function () {
    const {albums} = await getAlbums();
    let registro = "";
    for (let i = 0; i < albums.length; i++) {
    registro +=     `
                        
                            <tr scope="row">
                                <td>
                                    <p>${albums[i].nombre}</p>
                                </td>
                                <td>
                                    <p>${albums[i].fecha_publicacion}</p>
                                </td>
                                <td>
                                    <p>${albums[i].banda}</p>
                                </td>
                            </tr>
                        `;
        
    }
    console.log("funciona");
    document.querySelector('#table-bands').classList.add('disable');
    document.querySelector('#table-songs').classList.add('disable');
    document.querySelector('#table-albums').classList.remove('disable');

    document.querySelector('#body-albums').innerHTML = registro;
})

document.querySelector('#all-songs').addEventListener('click', async function () {
    const {songs} = await getSongs();
    console.log(songs);
    let registro = "";
    for (let i = 0; i < songs.length; i++) {
    registro +=     `
                            <tr scope="row">
                                <td>
                                    <p>${songs[i].nombre}</p>
                                </td>
                                <td>
                                    <p>${songs[i].duracion}</p>
                                </td>
                                <td>
                                    <p>${songs[i].album}</p>
                                </td>
                                <td>
                                    <p>${songs[i].banda}</p>
                                </td>
                            </tr>
                        `;
        
    }
    document.querySelector('#table-albums').classList.add('disable');
    document.querySelector('#table-bands').classList.add('disable');
    document.querySelector('#table-songs').classList.remove('disable');


    document.querySelector('#body-songs').innerHTML = registro;
})