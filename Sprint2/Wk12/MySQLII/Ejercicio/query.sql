CREATE TABLE bandasmusicales (
    id INT NOT NULL AUTO_INCREMENT,
    nombre varchar(100) NOT NULL,
    integrantes INT NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_separacion date NULL,
    pais varchar(100) NOT NULL,
    CONSTRAINT bandas_musicales_pk PRIMARY KEY (id)
);

CREATE TABLE canciones (
    id INT NOT NULL AUTO_INCREMENT,
    nombre varchar(100) NOT NULL,
    duracion INT NOT NULL,
    album INT NOT NULL,
    banda INT NOT NULL,
    fecha_publicacion date NOT NULL,
    CONSTRAINT canciones_id PRIMARY KEY (id)
);

CREATE TABLE albumes (
    id INT NOT NULL AUTO_INCREMENT,
    nombre varchar(100) NOT NULL,
    banda INT NOT NULL,
    fecha_publicacion DATE NOT NULL,
    CONSTRAINT albumes_id PRIMARY KEY (id)
);