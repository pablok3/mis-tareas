CREATE DATABASE acamica;

USE acamica;


CREATE TABLE acamica.bandamusical (
	id INTEGER auto_increment NOT NULL,
	nombre varchar(100) NOT NULL,
	integrantes INTEGER NOT NULL,
	fecha_inicio DATE NOT NULL,
	fecha_separacion DATE NULL,
	pais varchar(100) NOT NULL,
	CONSTRAINT bandamusical_pk PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;


CREATE TABLE acamica.canciones (
	id INTEGER auto_increment NOT NULL,
	nombre varchar(100) NOT NULL,
	duracion INTEGER NOT NULL,
	album INTEGER NOT NULL,
	banda INTEGER NOT NULL,
	fecha_publicacion DATE NOT NULL,
	CONSTRAINT canciones_pk PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;



CREATE TABLE acamica.albumes (
	id INTEGER auto_increment NOT NULL,
	nombre_album varchar(100) NOT NULL,
	banda INTEGER NOT NULL,
	fecha_publicacion DATE NOT NULL,
	CONSTRAINT albumes_pk PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

