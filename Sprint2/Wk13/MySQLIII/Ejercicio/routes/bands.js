const express = require('express');
const app = express();
const router = express.Router();
const config = require('../src/config/config');
const { validateFormBand } = require('../middlewares/bands')
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.db, config.user, config.pass, {
    host: config.host,
    dialect: 'mariadb'
});

app.use(express.json());


router.get('/:idBand', async (req, res) => {
    const {idBand} = req.params
    try {
        const band = await sequelize.query(`SELECT bm.nombre as nombre_banda, albumes.nombre, albumes.fecha_publicacion   FROM bandasmusicales bm INNER JOIN albumes ON bm.id = albumes.banda WHERE bm.id = ${idBand}`, {
            type: sequelize.QueryTypes.SELECT
        })
        if (band.length === 0) {
            res.status(400).json({"message": "No existe la banda"})
        } else{
            res.status(200).json({band});    
        }
    } catch (error) {
        res.status(400).json({"error": error.message})
    }
    
})

router.get('/', async (req, res) => {
    try {
        const bands = await sequelize.query(`SELECT * FROM bandasmusicales`, {
            type: sequelize.QueryTypes.SELECT
        })
        res.status(200).json({bands});
    } catch (error) {
        res.status(400).json({"error": error.message});
    }
    
})

router.post('/', validateFormBand, async (req, res) => {
    const {nombre, integrantes, fecha_inicio, fecha_separacion, pais} = req.body;
    try {
        const band = await sequelize.query(`INSERT INTO bandasmusicales (nombre, integrantes, fecha_inicio, fecha_separacion, pais) VALUES ("${nombre}",${integrantes}, "${fecha_inicio}", "${fecha_separacion}", "${pais}")`, { type: sequelize.QueryTypes.INSERT });
        res.status(201).json({band});
    } catch (error) {
        res.status(400).json({"error": error.message});
    }
})

router.put('/:idBand', async (req, res) => {
    const {idBand} = req.params;
    const {nombre, integrantes, fecha_inicio, fecha_separacion, pais} = req.body;
    try {
        const band = await sequelize.query(`UPDATE bandasmusicales SET nombre = '${nombre}', integrantes = ${integrantes}, fecha_inicio = '${fecha_inicio}', fecha_separacion = '${fecha_separacion}', pais = '${pais}' WHERE id = ${idBand}`, {
            type: sequelize.QueryTypes.UPDATE 
        });
        res.status(200).json({band});
    } catch (error) {
        res.status(400).json({"error": error.message})
    }
})

router.delete('/:idBand', async (req, res) => {
    const {idBand} = req.params;
    console.log(idBand);
        try {
            const band = await sequelize.query(`DELETE FROM bandasmusicales WHERE id = ${idBand}`, {
                type: sequelize.QueryTypes.DELETE 
            });
            res.status(200).json({band});
        } catch (error) {
            res.status(400).json({"error": error.message})
        }
    })

module.exports = router