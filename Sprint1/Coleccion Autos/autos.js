let autos = ['BMW','Peugeot','Fiat'];

function agregarAuto(){
    let marca = document.getElementById("autoNuevo").value;

    autos.push(marca);
    listar();
}

function eliminarPrimero(){

    autos.shift();
    listar();
}


function eliminarUltimo(){
    autos.pop();
    listar();
}


function listar(){

let autosHTML = "";
    for (i=0; i<autos.length;i++){
        autosHTML += "<li>" + autos[i] + "</li>";
    }

    document.getElementById("listaAutos").innerHTML = autosHTML;
    document.getElementById("autoNuevo").value = "";
}