let gama = ["Baja", "Media", "Alta"];

let celulares = [
    {name: "Samsung", gama: gama[2], modelo: "S10", pantalla: 6.6, sistemaOperativo: "Android 10", precio: 80000},
    {name: "LG", gama: gama[1], modelo: "K6", pantalla: 5.6, sistemaOperativo: "Android 8", precio: 35000},
    {name: "Huawei", gama: gama[2], modelo: "P9", pantalla: 6.1, sistemaOperativo: "Android 9", precio: 50000},
    {name: "Motorola", gama: gama[0], modelo: "E6", pantalla: 5, sistemaOperativo: "Android 7", precio: 30000},
    {name: "iPhone", gama: gama[1], modelo: "7", pantalla: 5.9, sistemaOperativo: "IOS9", precio: 60000},
    {name: "Nokia", gama: gama[0], modelo: "N95", pantalla: 3, sistemaOperativo: "Symbian 3", precio: 10000},
];


module.exports = {celulares, gama};