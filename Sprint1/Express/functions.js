const datos = require('./data')


function verMitad(){
    const mitad = (datos.celulares.length) / 2;
    const vMitad = datos.celulares.slice(0,mitad);
    return vMitad;
};

function menorPrecio(){                             
    datos.celulares.sort(function (x, y) {
        return x.precio - y.precio;
    });
    return datos.celulares[0];

// let min = Math.min(celulares.map(item => item.precio))           // Otra manera de hacerlo
// let menorPrecio = celulares.filter(item => item.precio === min) 
};

function mayorPrecio(){
    datos.celulares.sort(function (y, x) {
        return x.precio - y.precio;
    });
    return datos.celulares[0];

// let max = Math.max(celulares.map(item => item.precio))           // Otra manera de hacerlo
// let mayorPrecio = celulares.filter(item => item.precio === max)  
};


function gamas(){
    let filtrados = [];
        for (i = 0; i <= datos.gama.length; i++){
            let filtroGama = datos.celulares.filter(celu => celu.gama == datos.gama[i]);
            filtrados = filtrados.concat(filtroGama);
        };
    return filtrados;
}


module.exports = {verMitad, menorPrecio, mayorPrecio, gamas};