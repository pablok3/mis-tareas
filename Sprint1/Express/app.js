const config = require('./config')
const datos = require('./data');
const funcion = require('./functions');
var express = require('express');
var app = express();


// ----------------- Endpoints ------------------

app.get('/celulares', function (req, res) {
    res.send(datos.celulares)});


app.get('/mitadCelulares', function (req, res) {
    console.table(funcion.verMitad());
    res.send(funcion.verMitad())});

app.get('/menorPrecio', function (req, res) {
    console.table(funcion.menorPrecio());
    res.send(funcion.menorPrecio())});


app.get('/mayorPrecio', function (req, res) {
    console.table(funcion.mayorPrecio());
    res.send(funcion.mayorPrecio())});

    
app.get('/gamas', function (req, res) {
    console.table(funcion.gamas());
    res.send(funcion.gamas())});


// ----------------- Configuracion de puerto ------------------

app.listen(config.port, function () {
    console.log('Escuchando el puerto 3000!');
  });