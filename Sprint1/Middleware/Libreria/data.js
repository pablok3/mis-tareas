let libreria = [
    {
        id: 1,
        nombre: "Jorge Luis",
        apellido: "Borges",
        fechaDeNacimiento: "24/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1944
            },
            {
                id: 2,
                titulo: "El Aleph",
                descripcion: "Otra recopilacion de cuentos...",
                anioPublicacion: 1949
            }
        ]},{

        id: 2,
        nombre: "Stephen",
        apellido: "King",
        fechaDeNacimiento: "21/09/1947",
        libros: [
            {
                id: 1,
                titulo: "Carrie",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1974
            },
            {
                id: 2,
                titulo: "Eso (IT)",
                descripcion: "Otra recopilacion de cuentos...",
                anioPublicacion: 1986
            }
        ]
    }
];


module.exports = {libreria};