const config = require('./config');
const middle = require('./middleware');
const data = require('./data');
const express = require('express');
const app = express();
const compression = require('compression');
app.use(compression());
app.use(express.json());



// -------------------------------- Autores ---------------------------------

app.get('/autores', (req,res) => {
    let autores = []
    for(i =0; i < data.libreria.length; i++){
        autores.push(`Autor: ${data.libreria[i].nombre}, ${data.libreria[i].apellido}`)
    };
    res.json(autores);
});

//---------------------------

app.post('/autores', (req,res,next) => {
    data.libreria.push(req.body);
    res.json(`Autor ${req.body.nombre} ${req.body.apellido}, agregado!`)
});



// ------------------------------ Autores/:id  ---------------------------------

app.get('/autores/:id', middle.validarEscritor, (req,res) => {
    res.status(200).json(`${req.autor.nombre}, ${req.autor.apellido}`);
});

//---------------------------

app.delete('/autores/:id', middle.validarEscritor, (req,res) => {
    const indexAutor = data.libreria.indexOf(req.autor);
    data.libreria.splice(indexAutor, 1);

    res.status(201).json(`El autor ha sido eliminado!`)
});

//---------------------------

app.put('/autores/:id', middle.validarEscritor, (req,res) => {
    res.status(200).json(`Se ha modificado el autor ${req.autor.nombre} ${req.autor.apellido}`);
});



// --------------------------    Rutas / Libros   ----------------------------


app.get('/autores/:id/libros', middle.validarEscritor, (req, res) => {
    const autorIndex = data.libreria.indexOf(req.autor);
    res.status(200).json(data.libreria[autorIndex].libros);
});

//---------------------------

app.post('/autores/:id/libros', middle.validarEscritor, (req, res) => {
    req.autor.libros.push(req.body)
    res.status(200).json(`Nuevo libro agregado : ${req.autor.libros[req.autor.libros.length -1].titulo}`);
});



// -------------------------    Rutas / Libros/:Id    -------------------------


app.get('/autores/:id/libros/:idLibro', middle.validarEscritor, middle.validarLibro, (req, res) => {
    res.status(200).json(req.libro);
});

//---------------------------

app.put('/autores/:id/libros/:idLibro', middle.validarEscritor, middle.validarLibro, (req, res) => {
    res.status(201).json(`Se ha modificado el libro ${req.libro.titulo} del autor ${req.autor.nombre} ${req.autor.apellido}`);
});

//---------------------------

app.delete('/autores/:id/libros/:idLibro', middle.validarEscritor, middle.validarLibro, (req, res) => {
    req.autor.libros.pop(req.libro);
    res.status(201).json(`Se ha elminado el libro ${req.libro.titulo} del autor ${req.autor.nombre} ${req.autor.apellido}`)
});





// ---------------- Puerto -------------------
app.listen(config.port, () => console.log(`Escuchando puerto: ${config.port}`))