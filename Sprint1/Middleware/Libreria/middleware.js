const data = require('./data');


function validarEscritor(req, res, next) {
    const idAutor = parseInt(req.params.id);

    if (!Number.isInteger){
        res.status(422).json({ Alerta: "El id debe ser un número entero"});
        return;
    }else{
        let autor = data.libreria.find(autor => autor.id === idAutor);
        if (!autor) {
            res.status(400).json(`No se encuentra el autor con el ID ${idAutor}`);
        }else{
            req.autor = autor;
            next();
        }       
    }
};


function validarLibro(req, res, next){
    const idLibro = parseInt(req.params.idLibro);
    const libro = req.autor.libros.find(libro => libro.id === idLibro);
    
    if (!libro){
        res.status(400).json(`No se encuentra el libro con ID ${idLibro}`)
    }else{
        req.libro = libro;
        next();
    }
};

module.exports = {validarEscritor, validarLibro};