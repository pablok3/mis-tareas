window.addEventListener("resize", anchoPage);
let formulario_login = document.querySelector(".formulario__login");
let formulario_register = document.querySelector(".formulario__register");
let contenedor_login_register = document.querySelector(".contenedor__login-register");
let caja_trasera_login = document.querySelector(".caja__trasera-login");
let caja_trasera_register = document.querySelector(".caja__trasera-register");

anchoPage();
let usuarios = [];                                          // Creo array de Usuarios




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

document.getElementById("btn_registrarUsuario").addEventListener("click", function(){       // Funcion que comprueba que todos los campos sean ingresados, y las contraseñas iguales.

let nombre = document.getElementById('nombre').value;
let apellido = document.getElementById('apellido').value;
let email = document.getElementById('email').value;
let pais = document.getElementById('pais').value;
let pass = document.getElementById('pass').value;
let pass2 = document.getElementById('pass2').value;
    

if(nombre === "" || apellido === "" || email === "" || pais === "" || pass === ""){
    alert("Todos los campos son obligatorios")
    return;
}if(pass != pass2){
    alert("Las constraseñas deben ser iguales")
    return;
}

for (i = 0; i < usuarios.length; i++){                                                      // Si el email ya fue ingresado, avisa al usuario.
    if (email == usuarios[i].email){
    alert("El email ingresado ya se encuentra registrado")
    return;
    }
}

nuevoUsuario = new Usuario (nombre,apellido,email,pais,pass);                               // Crea el usuario nuevo a partir de la clase Usuario, y lo guarda en el array [usuarios]
usuarios.push(nuevoUsuario); 
alert("Usuario ingresado correctamente");
});
   

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

document.getElementById('btn_iniciarUsuario').addEventListener("click", function(){                    // Funcion que comprueba si existen el email y contraseñas ingresados previamente

let emailLogin = document.getElementById('emailLogin').value;
let passLogin = document.getElementById('passLogin').value;

let indiceUser = usuarios.findIndex(function(elemento){
    return elemento.email == emailLogin && elemento.pass == passLogin;
});


if (indiceUser != -1){
    alert("Bienvenido " + nuevoUsuario.nombre);
    return indiceUser;
}else{  
    alert("El email o contraseña no coinciden");
    return false;
    }
});



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

document.getElementById('btn_muestraLogin').addEventListener("click", function(){                    // Funcion que muestra el cuadro de Login

    if (window.innerWidth > 850){
            formulario_login.style.display = "block";
            contenedor_login_register.style.left = "10px";
            formulario_register.style.display = "none";
            caja_trasera_register.style.opacity = "1";
            caja_trasera_login.style.opacity = "0";
        }else{
            formulario_login.style.display = "block";
            contenedor_login_register.style.left = "0px";
            formulario_register.style.display = "none";
            caja_trasera_register.style.display = "block";
            caja_trasera_login.style.display = "none";
        }
    });   


document.getElementById('btn_muestraRegistro').addEventListener("click", function(){                // Funcion que muestra el cuadro de Registro

    if (window.innerWidth > 850){
        formulario_register.style.display = "block";
        contenedor_login_register.style.left = "410px";
        formulario_login.style.display = "none";
        caja_trasera_register.style.opacity = "0";
        caja_trasera_login.style.opacity = "1";
    }else{
        formulario_register.style.display = "block";
        contenedor_login_register.style.left = "0px";
        formulario_login.style.display = "none";
        caja_trasera_register.style.display = "none";
        caja_trasera_login.style.display = "block";
        caja_trasera_login.style.opacity = "1";
    }
});

function anchoPage(){                                               // Funcion que re acomoda el texto al actualizar la pagina

    if (window.innerWidth > 850){
        caja_trasera_register.style.display = "block";
        caja_trasera_login.style.display = "block";
    }else{
        caja_trasera_register.style.display = "block";
        caja_trasera_register.style.opacity = "1";
        caja_trasera_login.style.display = "none";
        formulario_login.style.display = "block";
        contenedor_login_register.style.left = "0px";
        formulario_register.style.display = "none";   
    }
}
