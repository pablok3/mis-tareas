const moment = require("moment");
const chalk = require("chalk");

const dateFormat = "YYYY-MM-DD HH:mm:ss";

let horarioLocal = moment().format(dateFormat);
let horarioUtc = moment.utc().format(dateFormat);
let diferenciaHoraria = moment(horarioUtc).diff(horarioLocal, 'hours');
let fechaA = "2021-04-10"
let fechaB = "2020-04-06"

let compararFecha = moment(fechaA).isAfter(fechaB);


console.log(chalk.blue(`Horario Local: ${horarioLocal}`));
console.log(chalk.bgGrey(`Horario UTC: ${horarioUtc}`));

console.log(`La diferencia entre el horario local y el horario UTC es de  ${chalk.blue.underline.bold(`${diferenciaHoraria} horas`)}`);



if (compararFecha){
    console.log(`La fecha ${chalk.blue.underline.bold(`A: ${fechaA}`)} es mayor a la fecha ${chalk.green.underline.bold(`B: ${fechaB}`)}`)
    }else{
    console.log(`La fecha ${chalk.blue.underline.bold(`A: ${fechaA}`)} es menor a la fecha ${chalk.green.underline.bold(`B: ${fechaB}`)}`)
    }