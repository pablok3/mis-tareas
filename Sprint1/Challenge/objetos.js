const miAuto = {
    marca : "Peugeot",
    modelo : "Partner",
    anyo : 2020,
    dominio : "AB123CD",
    _alquilado : false,

    alquilar: function (){
        if (this._alquilado){
            throw new Error ("No se puede alquilar");
        }
        this._alquilado = true;
    },
    devolver: function () {
        this._alquilado = false;
        return this._alquilado;
    }
}

const autoDeDolores = {
    marca : "Ford",
    modelo : "Fiesta",
    anyo : 2020,
    dominio : "AB123CD",
    _alquilado : false,

    alquilar: function (){
        this._alquilado = true;
      
    }
}


const garage = [miAuto,autoDeDolores];




do {
    let nuevoAuto = {}
    nuevoAuto.marca = prompt("Ingresa la marca de su auto");
    nuevoAuto.modelo = prompt("Ingresa el modelo de su auto");
    nuevoAuto.anyo = prompt("Ingresa el anyo de su auto");
    nuevoAuto.dominio = prompt("Ingresa el dominio de su auto");
    nuevoAuto._alquilado = false
    garage.push(nuevoAuto);
    

}while (window.confirm("Desea seguir agregando autos?"));


for (i=0 ; i<garage.length ; i++){
    const autoGarage = garage[i];

    publicarAuto(autoGarage);
}




function publicarAuto(auto){
    console.log(`Se vende el auto marca: ${auto.marca}, modelo ${auto.modelo}, anyo ${auto.anyo}, dominio ${auto.dominio}, estado de alquiler es: ${auto._alquilado}`)
}