///////////////////////////////////////////////

var condicionUno = (1 == "1");
var condicionDos = (1 === "1");
var condicionTres = (0 || 1);
var condicionCuatro = (true && 1);
var condicionCinco = (0) && (2+2);
var condicionSeis = (0) || (2+2);


console.log(condicionUno);
console.log(condicionDos);
console.log(condicionTres);
console.log(condicionCuatro);
console.log(condicionCinco);
console.log(condicionSeis);