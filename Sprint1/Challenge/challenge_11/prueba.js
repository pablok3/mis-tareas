var express = require('express');
var app = express();

app.get('/saludo', function (req, res) {
  res.send('Hola mundo');
});

app.get('/estadoCuenta', function (req, res) {
  res.send('Su saldo es de $150');
});


app.post('/users', function (req, res) {
  res.send('Usuario agregado correctamente');
});


app.delete('/users', function (req, res) {
  res.send('Usuario eliminado correctamente');
});


app.listen(3000, function () {
  console.log('Escuchando el puerto 3000!');
});