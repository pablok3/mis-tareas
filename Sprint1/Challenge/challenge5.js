let autos = ['BMW', 'Fiat', 'Renault']

for (i = 0; i < autos.length; i++) {
    //Imprime uno a uno todos los autos
console.log(autos[i]);
}

console.log("");
console.log("Ahora con While");
console.log("");

let indice = 0;

    while (indice < autos.length) {
        //Imprime uno a uno todos los autos
        // Estamos haciendo lo mismo que con el for
        console.log(autos[indice]);
        indice++;
  }  