class Estudiante {
    constructor(nombre, apellido, edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    fullname(){
        return `Mi nombre es ${this.nombre} ${this.apellido}`;
    }

    es_mayor(){
        (this.edad >= 18) ? "Es mayor de edad" : "No es mayor de edad";
    }
}

let pablo = new Estudiante ("Pablo","Secolo",17)

    console.log(pablo.fullname());
    pablo.es_mayor();