var env = require('./.env.variables.json');
require('dotenv').config();



const chalk = require('chalk');
const error = chalk.bold.red;
const warning = chalk.keyword('orange');


var node_env = process.env.node_env || 'development';

var variables = env[node_env];


console.log(chalk.blue(`El puerto de desarrollo es: ${variables.PORT}`));

console.log(chalk.blue.bgRed.bold('Hello world!'));
console.log(chalk.red('Hello', chalk.underline.bgBlue('world') + '!'));
console.log(chalk.green(
	'I am a green line ' +
	chalk.blue.underline.bold('with a blue substring') +
	' that becomes green again!'
));
console.log(`
CPU: ${chalk.red('90%')}
RAM: ${chalk.green('40%')}
DISK: ${chalk.yellow('70%')}
`);

console.log(error('Error!'));
console.log(warning('Warning!'));